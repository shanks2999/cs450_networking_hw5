"""
Where solution code to HW5 should be written.  No other files should
be modified.
"""

import socket
import io
import time
import typing
import struct
from struct import *
import homework5
import homework5.logging


# def get_timeout_send(estimated_rtt: float, dev_rtt: float, sample_rtt: float):
#     estimated_rtt = 0.875 * estimated_rtt + 0.125 * sample_rtt
#     dev_rtt = 0.75 * dev_rtt + 0.25 * abs(sample_rtt - estimated_rtt)
#     timeout = float("{0:.3f}".format(estimated_rtt + 4 * dev_rtt))
#     return timeout
#
# def get_timeout(estimated_rtt: float, dev_rtt: float, sample_rtt: float) -> float:
#     estimated_rtt = 0.875 * estimated_rtt + 0.125 * sample_rtt
#     dev_rtt = 0.75 * dev_rtt + 0.25 * abs(sample_rtt - estimated_rtt)
#     timeout = float("{0:.3f}".format(estimated_rtt + 4 * dev_rtt))
#     return timeout


def get_checksum_md5(data):
    import hashlib
    from hashlib import md5
    hash = hashlib.md5()
    hash.update(data)
    hash_string = hash.hexdigest()
    return hash_string


# def send(sock: socket.socket, data: bytes):
#     """
#     Implementation of the sending logic for sending data over a slow,
#     lossy, constrained network.
#
#     Args:
#         sock -- A socket object, constructed and initialized to communicate
#                 over a simulated lossy network.
#         data -- A bytes object, containing the data to send over the network.
#     """
#
#     # Naive implementation where we chunk the data to be sent into
#     # packets as large as the network will allow, and then send them
#     # over the network, pausing half a second between sends to let the
#     # network "rest" :)
#     logger = homework5.logging.get_logger("hw5-sender")
#     # chunk_size = homework5.MAX_PACKET
#     # pause = .1
#     # offsets = range(0, len(data), homework5.MAX_PACKET)
#     # for chunk in [data[i:i + chunk_size] for i in offsets]:
#     #     sock.send(chunk)
#     #     logger.info("Pausing for %f seconds", round(pause, 2))
#     #     time.sleep(pause)
#
#
#     estimated_rtt: float = 0
#     sample_rtt: float = 3
#     dev_rtt: float = 0
#     timeout: float = 0
#     start_time: float = 0
#     end_time: float = 0
#     chunk_size = 512 - 1
#     offsets = range(0, len(data), chunk_size)
#     compressed_bucket = []
#     index = 0
#     checkSum = ""
#     start_time = time.time()
#
#     for chunk in [data[i:i + chunk_size] for i in offsets]:
#         compressed_bucket.append(pack('s32s479s', index.to_bytes(1, byteorder='little'), str.encode(get_checksum_md5(chunk)), chunk))
#         index += 1
#
#     for i in range(len(compressed_bucket)):
#         index, checkSum, chunk = unpack('s32s479s', compressed_bucket[i])
#         index = int.from_bytes(index, byteorder='little')
#         print(str(index)+"<-->"+str(checkSum)+"<-->"+str(chunk))
#         print(len(compressed_bucket[i]))
#
#     end_time = time.time()
#     print(float("{0:.3f}".format(end_time - start_time)))
#
#
#
#     storage = []
#     sequence_number: int = 0
#     acknowledgement_number: int = 0
#     is_packet_loss = False
#     connection_teardown = False
#     pointer = 0
#
#     # pause = .1
#     # offsets = range(0, len(data), homework5.MAX_PACKET)
#     # for chunk in [data[i:i + chunk_size] for i in offsets]:
#     while True:
#         estimated_rtt = float("{0:.3f}".format(0.875 * estimated_rtt + 0.125 * sample_rtt))
#         dev_rtt = float("{0:.3f}".format(0.75 * dev_rtt + 0.25 * abs(sample_rtt - estimated_rtt)))
#         timeout = float("{0:.3f}".format(estimated_rtt + 4 * dev_rtt))
#         sock.settimeout(timeout)
#         # sock.settimeout(get_timeout_send(estimated_rtt, dev_rtt, sample_rtt))
#         # print(str(estimated_rtt)+" <--> "+str(sample_rtt)+" <--> "+str(dev_rtt)+" <--> "+str(timeout))
#         try:
#             if not is_packet_loss:
#                 if pointer < len(data):
#                     packet = data[pointer:pointer + chunk_size]
#                     packet = sequence_number.to_bytes(1, byteorder='little', signed=True) + packet
#                     storage.append(packet)
#                     sock.send(packet)
#                     sequence_number += 1
#                     pointer += chunk_size
#
#                 elif pointer >= len(data) and acknowledgement_number == sequence_number:
#                     sock.send(b'FIN')
#             elif is_packet_loss and acknowledgement_number < len(storage):
#                 packet = storage[acknowledgement_number]
#                 sock.send(packet)
#             start_time = time.time()
#             response = sock.recv(10)
#             end_time = time.time()
#             sample_rtt = float("{0:.3f}".format(end_time - start_time))
#             if response == b'ACK_FIN':
#                 sock.send(b'ACK')
#                 estimated_rtt = float("{0:.3f}".format(0.875 * estimated_rtt + 0.125 * sample_rtt))
#                 dev_rtt = float("{0:.3f}".format(0.75 * dev_rtt + 0.25 * abs(sample_rtt - estimated_rtt)))
#                 timeout = float("{0:.3f}".format(estimated_rtt + 4 * dev_rtt))
#                 sock.settimeout(timeout)
#                 connection_teardown = True
#                 sock.recv(homework5.MAX_PACKET)
#             else:
#                 acknowledgement_number = int(response.decode()[4:])
#                 if acknowledgement_number == sequence_number:
#                     is_packet_loss = False
#                 else:
#                     is_packet_loss = True
#         except socket.timeout:
#             if connection_teardown:
#                 break
#             if is_packet_loss:
#                 if acknowledgement_number < len(storage) and acknowledgement_number < sequence_number:
#                     acknowledgement_number += 1
#                 elif acknowledgement_number == sequence_number:
#                     is_packet_loss = False
#             if sample_rtt < 3:
#                 sample_rtt *= 1.5
#             else:
#                 sample_rtt *= 0.75
#             sample_rtt = float("{0:.3f}".format(sample_rtt))
#
#
#         # logger.info("Pausing for %f seconds", round(pause, 2))
#         # time.sleep(pause)

def send(sock: socket.socket, data: bytes):
    """
    Implementation of the sending logic for sending data over a slow,
    lossy, constrained network.

    Args:
        sock -- A socket object, constructed and initialized to communicate
                over a simulated lossy network.
        data -- A bytes object, containing the data to send over the network.
    """

    # Naive implementation where we chunk the data to be sent into
    # packets as large as the network will allow, and then send them
    # over the network, pausing half a second between sends to let the
    # network "rest" :)
    logger = homework5.logging.get_logger("hw5-sender")
    estimated_rtt: float = 1
    sample_rtt: float = 1
    dev_rtt: float = 0
    timeout: float = 0
    start_time: float = 0
    end_time: float = 0
    is_connection_teardown = False
    sequence_number: int = 0
    acknowledgement_number: int = 0
    chunk_size = 1400 - 32 - 200
    compressed_bucket = []
    for chunk in [data[i:i + chunk_size] for i in range(0, len(data), chunk_size)]:
        compressed_bucket.append(sequence_number.to_bytes(18, byteorder='big') + str.encode(get_checksum_md5(chunk)) + chunk)
        sequence_number += 1
    sequence_number: int = 0
    duplicate_ack = -1
    duplicate_ack_count = 0
    cwnd = 1
    is_timeout = False
    custom_pointer = 1
    while True:
        # print(custom_pointer)
        # custom_pointer += 1
        dev_rtt = sample_rtt/2
        estimated_rtt = float("{0:.3f}".format(0.875 * estimated_rtt + 0.125 * sample_rtt))
        dev_rtt = float("{0:.3f}".format(0.75 * dev_rtt + 0.25 * abs(sample_rtt - estimated_rtt)))
        timeout = float("{0:.3f}".format(estimated_rtt + 4 * dev_rtt))
        sock.settimeout(timeout)
        # print(str(estimated_rtt)+" <--> "+str(sample_rtt)+" <--> "+str(dev_rtt)+" <--> "+str(timeout))
        try:
            if sequence_number < len(compressed_bucket):
                if sequence_number < len(compressed_bucket)-1:
                    packet_one = compressed_bucket[sequence_number]
                    packet_two = compressed_bucket[sequence_number+1]
                else:
                    packet_one = compressed_bucket[sequence_number]
                    packet_two = compressed_bucket[sequence_number]
                sock.send(packet_one)
                sock.send(packet_two)
            elif sequence_number >= len(compressed_bucket) and acknowledgement_number == len(compressed_bucket):
                print("SENDER OUT!!!")
                break

            try:
                start_time = time.time()
                response = sock.recv(homework5.MAX_PACKET)
                end_time = time.time()
                sample_rtt = float("{0:.3f}".format(end_time - start_time))
                acknowledgement_number = int.from_bytes(response[4:], byteorder='big')
                is_timeout = False
            except socket.timeout:
                is_timeout = True

            if not is_timeout:
                if duplicate_ack == acknowledgement_number:
                        duplicate_ack_count += 1
                else:
                    duplicate_ack = acknowledgement_number
                    duplicate_ack_count = 0

                if duplicate_ack_count >= 3:
                    sequence_number = acknowledgement_number
                else:
                    sequence_number += 2
        except socket.timeout:
            if sample_rtt < 3:
                sample_rtt *= 1.5
            else:
                sample_rtt *= 0.75
            sample_rtt = float("{0:.3f}".format(sample_rtt))

    """Hack"""
    for i in range(50):
        sock.send(b'FIN')

    """This is connection Tear Down from Sender"""
    # while True:
    #     sock.send(b'FIN')
    #     try:
    #         while True:
    #             start_time = time.time()
    #             response = sock.recv(homework5.MAX_PACKET)
    #             end_time = time.time()
    #             sample_rtt = float("{0:.3f}".format(end_time - start_time))
    #             dev_rtt = sample_rtt / 2
    #             estimated_rtt = float("{0:.3f}".format(0.875 * estimated_rtt + 0.125 * sample_rtt))
    #             dev_rtt = float("{0:.3f}".format(0.75 * dev_rtt + 0.25 * abs(sample_rtt - estimated_rtt)))
    #             timeout = float("{0:.3f}".format(estimated_rtt + 4 * dev_rtt))
    #             sock.settimeout(timeout)
    #             if response == b'FIN' or response == b'ACK':
    #                 break
    #     except socket.timeout:
    #         if sample_rtt < 3:
    #             sample_rtt *= 1.5
    #         else:
    #             sample_rtt *= 0.75
    #         sample_rtt = float("{0:.3f}".format(sample_rtt))
    #         continue
    #     sock.send(b'ACK')
    #     try:
    #         while True:
    #             start_time = time.time()
    #             response = sock.recv(homework5.MAX_PACKET)
    #             end_time = time.time()
    #             sample_rtt = float("{0:.3f}".format(end_time - start_time))
    #             dev_rtt = sample_rtt / 2
    #             estimated_rtt = float("{0:.3f}".format(0.875 * estimated_rtt + 0.125 * sample_rtt))
    #             dev_rtt = float("{0:.3f}".format(0.75 * dev_rtt + 0.25 * abs(sample_rtt - estimated_rtt)))
    #             timeout = float("{0:.3f}".format(estimated_rtt + 4 * dev_rtt))
    #             sock.settimeout(timeout)
    #             if response != b'ACK' and response != b'FIN':
    #                 break
    #     except socket.timeout:
    #         break
        # logger.info("Pausing for %f seconds", round(pause, 2))
        # time.sleep(pause)

def recv(sock: socket.socket, dest: io.BufferedIOBase) -> int:
    """
    Implementation of the receiving logic for receiving data over a slow,
    lossy, constrained network.

    Args:
        sock -- A socket object, constructed and initialized to communicate
                over a simulated lossy network.

    Return:
        The number of bytes written to the destination.
    """
    logger = homework5.logging.get_logger("hw5-receiver")
    # Naive solution, where we continually read data off the socket
    # until we don't receive any more data, and then return.
    estimated_rtt: float = 1
    sample_rtt: float = 1
    dev_rtt: float = 0
    timeout: float = 0
    start_time: float = 0
    end_time: float = 0

    sqn_number: int = -1
    ack_number: int = sqn_number + 1
    num_bytes = 0
    while True:
        dev_rtt = sample_rtt / 2
        estimated_rtt = float("{0:.3f}".format(0.875 * estimated_rtt + 0.125 * sample_rtt))
        dev_rtt = float("{0:.3f}".format(0.75 * dev_rtt + 0.25 * abs(sample_rtt - estimated_rtt)))
        timeout = float("{0:.3f}".format(estimated_rtt + 4 * dev_rtt))
        sock.settimeout(timeout)
        try:
            response_one = None
            start_time = time.time()
            response_one = sock.recv(homework5.MAX_PACKET)
            end_time = time.time()
            sample_rtt = float("{0:.3f}".format(end_time - start_time))

            if response_one == b'FIN':
                print("RECEIVER OUT!!!")
                break
            # sqn_number, checksum, packet = unpack('18s32s1350s', response_one)
            sqn_number = int.from_bytes(response_one[:18], byteorder='big')
            checksum = response_one[18:50]
            packet = response_one[50:]
            # packet = packet.partition(b'\0')[0]
            # packet = packet.rstrip(b'\0')
            if ack_number != sqn_number or get_checksum_md5(packet) != checksum.decode():
                raise socket.timeout
            ack_number = sqn_number + 1
            # sock.send(b'ACK_' + ack_number.to_bytes(1, byteorder='little'))
            logger.info("Received %d bytes", len(packet))
            dest.write(packet)
            num_bytes += len(packet)
            dest.flush()

            response_two = None
            start_time = time.time()
            response_two = sock.recv(homework5.MAX_PACKET)
            end_time = time.time()
            sample_rtt = float("{0:.3f}".format(end_time - start_time))

            if response_two == b'FIN':
                print("RECEIVER OUT!!!")
                break
            # sqn_number, checksum, packet = unpack('i32s1168s', response_two)
            sqn_number = int.from_bytes(response_two[:18], byteorder='big')
            checksum = response_two[18:50]
            packet = response_two[50:]
            # packet = packet.partition(b'\0')[0]
            # packet = packet.rstrip(b'\0')
            # sqn_number = int.from_bytes(sqn_number, byteorder='little')
            if ack_number != sqn_number or get_checksum_md5(packet) != checksum.decode():
                raise socket.timeout
            ack_number = sqn_number + 1
            # print(len(packet))
            # sock.send(b'ACK_' + ack_number.to_bytes(1, byteorder='little'))
            # print("Sent ack")
            logger.info("Received %d bytes", len(packet))
            dest.write(packet)
            num_bytes += len(packet)
            dest.flush()
            ack_number.to_bytes(18, byteorder='big')
            sock.send(b'ACK_' + ack_number.to_bytes(18, byteorder='big'))
        except socket.timeout:
            if sample_rtt < 3:
                sample_rtt *= 1.5
            else:
                sample_rtt *= 0.75
            sample_rtt = float("{0:.3f}".format(sample_rtt))
            sock.send(b'ACK_' + ack_number.to_bytes(18, byteorder='big'))
            # sock.send(b'ACK_' + str.encode(str(ack_number)))
            # print("Sent ack from exception")

    """This is connection Tear Down from Receiver"""
    # while True:
    #     sock.send(b'ACK')
    #     sock.send(b'FIN')
    #     try:
    #         start_time = time.time()
    #         response = sock.recv(homework5.MAX_PACKET)
    #         end_time = time.time()
    #         sample_rtt = float("{0:.3f}".format(end_time - start_time))
    #         dev_rtt = sample_rtt / 2
    #         estimated_rtt = float("{0:.3f}".format(0.875 * estimated_rtt + 0.125 * sample_rtt))
    #         dev_rtt = float("{0:.3f}".format(0.75 * dev_rtt + 0.25 * abs(sample_rtt - estimated_rtt)))
    #         timeout = float("{0:.3f}".format(estimated_rtt + 4 * dev_rtt))
    #         sock.settimeout(timeout)
    #         if response == b'ACK':
    #             break
    #     except socket.timeout:
    #         continue

    return num_bytes


# def recv(sock: socket.socket, dest: io.BufferedIOBase) -> int:
#     """
#     Implementation of the receiving logic for receiving data over a slow,
#     lossy, constrained network.
#
#     Args:
#         sock -- A socket object, constructed and initialized to communicate
#                 over a simulated lossy network.
#
#     Return:
#         The number of bytes written to the destination.
#     """
#     logger = homework5.logging.get_logger("hw5-receiver")
#     # Naive solution, where we continually read data off the socket
#     # until we don't receive any more data, and then return.
#     # num_bytes = 0
#     # while True:
#     #     data = sock.recv(homework5.MAX_PACKET)
#     #     if not data:
#     #         break
#     #     logger.info("Received %d bytes", len(data))
#     #     dest.write(data)
#     #     num_bytes += len(data)
#     #     dest.flush()
#     # return num_bytes
#
#
#     # chunk_size = homework5.MAX_PACKET
#     # pause = .1
#     # offsets = range(0, len(data), homework5.MAX_PACKET)
#     # for chunk in [data[i:i + chunk_size] for i in offsets]:
#     #     sock.send(chunk)
#     #     response = sock.recv(homework5.MAX_PACKET)
#     #     logger.info("Pausing for %f seconds", round(pause, 2))
#     #     time.sleep(pause)
#     estimated_rtt: float = 0
#     sample_rtt: float = 3
#     dev_rtt: float = 0
#     timeout: float = 0
#     start_time: float = 0
#     end_time: float = 0
#
#     sqn_number: int = -1
#     ack_number: int = sqn_number + 1
#     num_bytes = 0
#     while True:
#         estimated_rtt = float("{0:.3f}".format(0.875 * estimated_rtt + 0.125 * sample_rtt))
#         dev_rtt = float("{0:.3f}".format(0.75 * dev_rtt + 0.25 * abs(sample_rtt - estimated_rtt)))
#         timeout = float("{0:.3f}".format(estimated_rtt + 4 * dev_rtt))
#         sock.settimeout(timeout)
#         try:
#             start_time = time.time()
#             data = sock.recv(512)
#             end_time = time.time()
#             sample_rtt = float("{0:.3f}".format(end_time - start_time))
#             # print("Received data")
#             if data == b'FIN':
#                 sock.send(b'ACK_FIN')
#                 estimated_rtt = float("{0:.3f}".format(0.875 * estimated_rtt + 0.125 * sample_rtt))
#                 dev_rtt = float("{0:.3f}".format(0.75 * dev_rtt + 0.25 * abs(sample_rtt - estimated_rtt)))
#                 timeout = float("{0:.3f}".format(estimated_rtt + 4 * dev_rtt))
#                 sock.settimeout(timeout)
#                 start_time = time.time()
#                 data = sock.recv(10)
#                 end_time = time.time()
#                 sample_rtt = float("{0:.3f}".format(end_time - start_time))
#                 if data == b'ACK':
#                     break
#             else:
#                 packet = data[1:]
#                 sqn_number = int.from_bytes(data[0:1], byteorder='little', signed=True)
#                 if ack_number != sqn_number:
#                     raise socket.timeout
#                 ack_number = sqn_number + 1
#                 sock.send(b'ACK_' + str(ack_number).encode('utf-8'))
#                 # print("Sent ack")
#                 logger.info("Received %d bytes", len(packet))
#                 dest.write(packet)
#                 num_bytes += len(packet)
#                 dest.flush()
#         except socket.timeout:
#             if sample_rtt < 3:
#                 sample_rtt *= 1.5
#             else:
#                 sample_rtt *= 0.75
#             sample_rtt = float("{0:.3f}".format(sample_rtt))
#             sock.send(b'ACK_' + str(ack_number).encode('utf-8'))
#             # print("Sent ack from exception")
#
#     return num_bytes
